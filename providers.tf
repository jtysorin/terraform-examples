terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.13.1"
    }

    linode = {
        source = "linode/linode"
        version = "2.6.0"
    }
  }
}