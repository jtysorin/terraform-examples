# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "5.13.1"
  hashes = [
    "h1:uYJsEJhxGO/dFeK2MsC65wV/NKu7XxnTikh5rbrizBo=",
    "zh:0d107e410ecfbd5d2fb5ff9793f88e2ce03ae5b3bda4e3b772b5d146cdd859d8",
    "zh:1080cf6a402939ec4ad393380f2ab2dfdc0e175903e08ed796aa22eb95868847",
    "zh:300420d642c3ada48cfe633444eafa7bcd410cd6a8503de2384f14ac54dc3ce3",
    "zh:4e0121014a8d6ef0b1ab4634877545737bb54e951340f1b67ffea8cd22b2d252",
    "zh:59b401bbf95dc8c6bea58085ff286543380f176271251193eac09cb7fcf619b7",
    "zh:5dfaf51e979131710ce8e1572e6012564e68c7c842e3d9caaaeb0fe6af15c351",
    "zh:84bb75dafca056d7c3783be5185187fdd3294f902e9d72f7655f2efb5e066650",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:aa4e2b9f699d497041679bc05ca34ac21a5184298eb1813f35455b1883977910",
    "zh:b51a4f08d84b071128df68a95cfa5114301a50bf8ab8e655dcf7e384e5bc6458",
    "zh:bce284ac6ebb65053d9b703048e3157bf4175782ea9dbaec9f64dc2b6fcefb0c",
    "zh:c748f78b79b354794098b06b18a02aefdb49be144dff8876c9204083980f7de0",
    "zh:ee69d9aef5ca532392cdc26499096f3fa6e55f8622387f78194ebfaadb796aef",
    "zh:ef561bee58e4976474bc056649095737fa3b2bcb74602032415d770bfc620c1f",
    "zh:f696d8416c57c31f144d432779ba34764560a95937db3bb3dd2892a791a6d5a7",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "2.6.0"
  constraints = "2.6.0"
  hashes = [
    "h1:LqjPjObIRhj0RQXB7qCicGs8CHlTuE8Ylx3+aNAXPBk=",
    "zh:414f801f430af8bc87e2a57d0dad3cae3ad599e536bb885efb69a0d47a70c33c",
    "zh:49c296afe4ccaea5745f9fe1422e5fd4f61df3901aa53c74dfdf59eca5fccb1a",
    "zh:4f0d9ca2554ced3a532128698b2fa2b7368c517d615e84ac7722a47ed5572dbc",
    "zh:6873873babcf00e5481c0c5ee5597fce676ae6770ed92f716779c4d83e5a7dcc",
    "zh:76ce49e0d4fd436e6c6f98a12882a08b04f9ae0479189a75b582195a81e57fbe",
    "zh:840ebe7d421df1b1105670ead1a41d4bf0ff6c03a6e879d59cfcb2af2726754d",
    "zh:975fa4dacc60aa803e56d40cb70f5c5cc5d9ee32479ab052d9fac546a551efe0",
    "zh:ba4da65b8f2e1f6f30df3fc0e0da6a915aaaf90617e1a50f16ba42a3897118fd",
    "zh:cd7edd9d5e22353dab98c5e8b81910ffcc34979c495f3ac8847faf87c71a36b0",
    "zh:d8c87a38c8cbe931ee37ee08001357aa8643a368ae7c003c1881d1ce3983bac3",
    "zh:dd510acd4c1fba6d31be1054712c78b68f25dde3f04cc6909f27834f225c608d",
    "zh:e404c6334d8eac79b34653bcfd4932e80e037fadc3af8e5ab0d9948eefada0ad",
    "zh:e4dc0fde2fba1ef82c4343d21d5cf1396e67b476349f026a814e4816604f47a8",
    "zh:ff303954749404b570a83438a506e4e2640bc16af991aa241e5b6146ffc8fd31",
  ]
}
